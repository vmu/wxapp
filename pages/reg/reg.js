// pages/reg/reg.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    username:"sscc@88.com",
    password:"123456",
    phone:"7246",
  },
  bindinputUsername:function(e){
    console.log(e.detail.value)
    this.setData({
      username:e.detail.value
    })
  },
  //简化版
  bindinput:function (e) {//输入事件处理函数
    var field = e.target.dataset.name//获取文本框的名字
    console.log(field)
    var userData = {}//构建数据结构
    userData[field] = e.detail.value//存入内容
    this.setData(userData)
    // console.log(userData);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})