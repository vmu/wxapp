// pages/goods/goods.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
      name:"华硕手提笔记本电脑轻薄便携学生办公用商务电竞吃鸡游戏本i7高配",
      price:"¥1100.00-5080.00",
      address:"https://gd1.alicdn.com/imgextra/i1/64796146/O1CN013qZmQv1vGupWXsmSe_!!64796146.jpg",
      isShow:true,
      repertory:5,

      goods:[
        {id:1,name:"Thinkpad联想笔记本电脑轻薄便携学生i7游戏本办公用商务超薄手提",
        price:"¥1450.00-3529.00",
        address:"https://gd3.alicdn.com/imgextra/i2/1072551970/O1CN017R83C51QQIu2IzYeP_!!1072551970.jpg",
        isShow:true,
        repertory:20,
        },
        {id:2,name:"HP/惠普笔记本电脑轻薄便携学生超薄 游戏本i7独显高配置办公手提",
        price:"¥1450.00-2946.00",
        address:"https://gd2.alicdn.com/imgextra/i2/1863119483/O1CN01dFO39E2JvGXWTzDIe_!!1863119483.jpg",
        isShow:true,
        repertory:20,
        }
      ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    //1模拟从服务器加载下一页数据（模拟）
    var remotGoods=[
      {id:1,name:"Thinkpad联想笔记本电脑轻薄便携学生i7游戏本办公用商务超薄手提",
      price:"¥1450.00-3529.00",
      address:"https://gd3.alicdn.com/imgextra/i2/1072551970/O1CN017R83C51QQIu2IzYeP_!!1072551970.jpg",
      isShow:true,
      repertory:20,
      },
      {id:2,name:"HP/惠普笔记本电脑轻薄便携学生超薄 游戏本i7独显高配置办公手提",
      price:"¥1450.00-2946.00",
      address:"https://gd2.alicdn.com/imgextra/i2/1863119483/O1CN01dFO39E2JvGXWTzDIe_!!1863119483.jpg",
      isShow:true,
      repertory:20,
      }
    ]
    //2将加载的数据追加到原来的商品列表,contact表示连接 
    var newGoods = this.data.goods.concat(remotGoods)
    //3将原来的数组更新为新的数组
    this.setData({
      goods:newGoods
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})